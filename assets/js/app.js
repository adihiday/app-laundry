$(document).ready(function(){

	var page = $('#pages_property').attr('page');
	if ( page == 'employees') employees();
	else if( page == 'outlets') outlets();

	//Semua page dapat mengakses kode berikut :


	//end

	//Fungsi ini hanya berjalan di page yang di load / aktif.
	function employees()
	{
		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		$('#table_employees').DataTable({

			oLanguage: {
				sProcessing: "<i class='fa fa-spinner fa-spin'></i> Loading Data...",
				oPaginate: {
					"sNext": "<i class='fa fa-arrow-right'></i>",
					"sPrevious" : "<i class='fa fa-arrow-left'></i>"
				}
			},

			paging: true,
			searching: false,
			processing: true,
			serverSide: true,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],

			ajax: {
				"url": $('#pages_property').attr('ajax_url'),
				"type": "POST",
				"data": function (data) {
					data.param = $('#search_employee').val();
				}
			},
			columns: [
				{
					"data": "employee_id", "orderable" : false
				},
				{
					"data": "employee_name"
				},
				{
					"data": "employee_phone"
				},
				{
					"data": "outlet_name"
				},
				{
					"data": "employee_gender"
				},
				{
					"data": "employee_address"
				},
				{
					"data": "action", "className" : "text-right", "orderable" : false,
				}
			],
			order: ['0','asc'],
			rowId: function (a) {
				return a;
			},
			rowCallback: function (row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});

		$('#search_employee').on('keyup', function(){
			if($('#search_employee').attr('active-employee', true)){
				$('#table_employees').DataTable().ajax.reload();
				console.log('true');
			}else if($('#search_employee').prop('active-employee', false)){
				$('#table_employees').DataTable().ajax.url( $('#pages_property').attr('ajax_url') + 'deleted/' + data ).load();
				console.log('false');
			}
		});

		$('#deleted_employees').on('click', function(e){
			e.preventDefault();
			$('#search_employee').attr('active-employee',false);
			$('#hot_info').removeClass('hidden');
			$('#hot_info').fadeIn('medium');
			$('#table_employees').DataTable().ajax.url( $('#pages_property').attr('ajax_url') + 'deleted' ).load();;
			$('#action').prop('hidden', true);
		});

		$('#active_employees').on('click', function(e){
			e.preventDefault();
			$('#search_employee').attr('active-employee',true);
			$('#hot_info').addClass('hidden');
			$('#hot_info').fadeOut('fast');
			$('#table_employees').DataTable().ajax.url( $('#pages_property').attr('ajax_url')).load();;
			$('#action').prop('hidden', false);
		});

		$('#add_employee').on('click', function(){
			$('.error_validation').empty();
			$('.input-radio', '#gender').prop('checked', false);
			$('.form-control').val('');
		});

		$('#save_employee_data').on('click', function(){
			var employee_id = $('#employee_id').val();
			var form = $('#form_employee');
			$.ajax({
				url : $('#form_employee').attr('action') + employee_id,
				type : 'POST',
				data : form.serialize(),
				dataType: 'JSON',
				success:function(data){
					$('.error_validation').empty();
					if(data.result == true){
						$('#table_employees').DataTable().ajax.reload();
						$('#employeeModal').modal('hide');
						if(employee_id != ''){
							toastr["info"]("Data Berhasil Diperbaharui", "Sukses")
						}else{
							toastr["info"]("Data Berhasil Ditambahkan", "Sukses")
						}
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": true,
							"positionClass": "toast-bottom-right",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
						$('.form-control').val('');
					}else if(data.result ==  false){
						alert('Somethiing when wrong');
					}else{
						$('#nik').text(data.employee_id_card);
						$('#kk').text(data.employee_id_kk);
						$('#full_name').text(data.employee_name);
						$('#phone').text(data.employee_phone);
						$('#gender').text(data.employee_gender);
						$('#birth_place').text(data.place_birth);
						$('#birth_date').text(data.date_birth);
						$('#address').text(data.employee_address);
					}
				},
				error:function(data){
					console.log(data);
					alert('failed');
				}
			});
		});
		
		$('#table_employees').on('click','#edit_employee', function(e){
			e.preventDefault();
			$('.input-radio').prop('checked', false);
			$.ajax({
				url : $(this).attr('href'),
				type : 'GET',
				dataType : 'JSON',
				success:function(data){
					var i;
					for(i=0; i<data.length; i++){
						console.log(data[i].employee_id_card);
						$('#employee_id').val(data[i].employee_id);
						$('#input_employee_id_card').val(data[i].employee_id_card);
						$('#input_id_kk').val(data[i].employee_id_kk);
						$('#input_full_name').val(data[i].employee_name);
						$('#input_phone').val(data[i].employee_phone);
						$('#input_email').val(data[i].employee_email);
						$('#input_place_birth').val(data[i].employee_birth_place);
						$('#input_date_birth').val(data[i].employee_birth_date);
						$('#input_address').val(data[i].employee_address);
						if(data[i].employee_gender == "Laki-laki"){
							$('#male_gender').prop('checked', true);
						}else if(data[i].employee_gender == "Perempuan"){
							$('#female_gender').prop('checked', true);
						}
					}
				},
				error:function(data){
					console.log(data);
				}
			});
			$('#employeeModal').modal('show');
		});

		$('#table_employees').on('click','#delete_employee', function(e){
			e.preventDefault();
			$.ajax({
				url : $(this).attr('href'),
				type : 'GET',
				dataType : 'JSON',
				success:function(data){
					$('#table_employees').DataTable().ajax.reload();
					toastr["info"]("Data Berhasil Dihapus", "Sukses")
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-bottom-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				},
				error:function(data){
					toastr["danger"]("Gagal menghapus data", "Galat")
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-bottom-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}
			});
		});

		$('#table_employees').on('click','#restore_employee', function(e){
			e.preventDefault();
			$.ajax({
				url : $(this).attr('href'),
				type : 'GET',
				dataType : 'JSON',
				success:function(data){
					$('#table_employees').DataTable().ajax.reload();
					toastr["info"]("Data Berhasil Dihapus", "Sukses")
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-bottom-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				},
				error:function(data){
					toastr["danger"]("Gagal menghapus data", "Galat")
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": true,
						"positionClass": "toast-bottom-right",
						"preventDuplicates": false,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}
			});
		});
	}

	function outlets()
	{
		$('#add_employee').on('click', function(){
			$('.error_validation').empty();
			$('.input-radio', '#gender').prop('checked', false);
			$('.form-control').val('');
		});

		$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		$('#table_outlets').DataTable({

			oLanguage: {
				sProcessing: "<i class='fa fa-spinner fa-spin'></i> Loading Data...",
				oPaginate: {
					"sNext": "<i class='fa fa-arrow-right'></i>",
					"sPrevious" : "<i class='fa fa-arrow-left'></i>"
				}
			},

			paging: true,
			searching: false,
			processing: true,
			serverSide: true,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],

			ajax: {
				"url": $('#pages_property').attr('ajax_url'),
				"type": "POST",
				"data": function (data) {
					data.start_date = $('#start_date').val();
					data.end_date = $('#end_date').val();
					data.status = $('#order_status').val();
					data.order_type = $('#src_order_type').val();
					data.destination = $('#src_country').val();
					data.search_param = $('#search_param').val();
				}
			},
			columns: [
			{
				"data": "employee_id", "orderable" : false
			},
			{
				"data": "employee_name"
			},
			{
				"data": "employee_phone"
			},
			{
				"data": "outlet_name"
			},
			{
				"data": "employee_gender"
			},
			{
				"data": "employee_address"
			},
			{
				"data": "employee_id", "className" : "text-right", "orderable" : false,
				render: function(data, type, row, meta) {
					return '<a class="btn btn-success btn-sm" id="edit_employee" href="' + $('#pages_property').attr('ajax_url') + data +'">Edit</a><a class="btn btn-primary btn-sm" href="' + $('#pages_property').attr('ajax_url') + data +'">Detail</a><a id="delete_employee" class="btn btn-danger btn-sm" href="' + 'delete/' + data + '">Hapus</a>'
				}
			}
			],
			order: ['0','asc'],
			rowId: function (a) {
				return a;
			},
			rowCallback: function (row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});

		$('#save_outlet_data').on('click', function(){
			var employee_id = $('#employee_id').val();
			var form = $('#form_employee');
			$.ajax({
				url : $('#form_outlet').attr('action') + employee_id,
				type : 'POST',
				data : form.serialize(),
				dataType: 'JSON',
				success:function(data){
					$('.error_validation').empty();
					if(data.result == true){
						$('#table_employees').DataTable().ajax.reload();
						$('#employeeModal').modal('hide');
						if(employee_id != ''){
							toastr["info"]("Data Outlet Berhasil Diperbaharui", "Sukses")
						}else{
							toastr["info"]("Data Outlet Berhasil Ditambahkan", "Sukses")
						}
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"newestOnTop": false,
							"progressBar": true,
							"positionClass": "toast-bottom-right",
							"preventDuplicates": false,
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
						$('.form-control').val('');
					}else if(data.result ==  false){
						alert('Somethiing when wrong');
					}else{
						$('#nik').text(data.employee_id_card);
						$('#kk').text(data.employee_id_kk);
						$('#full_name').text(data.employee_name);
						$('#phone').text(data.employee_phone);
						$('#gender').text(data.employee_gender);
						$('#birth_place').text(data.place_birth);
						$('#birth_date').text(data.date_birth);
						$('#address').text(data.employee_address);
					}
				},
				error:function(data){
					console.log(data);
					alert('failed');
				}
			});
		});

		$('#table_outlets').on('click','#edit_outlet', function(e){
			e.preventDefault();
			$.ajax({
				url : $(this).attr('href'),
				type : 'GET',
				dataType : 'JSON',
				success:function(data){
					var i;
					for(i=0; i<data.length; i++){
						console.log(data[i].employee_id_card);
						$('#employee_id').val(data[i].employee_id);
						$('#input_employee_id_card').val(data[i].employee_id_card);
						$('#input_id_kk').val(data[i].employee_id_kk);
						$('#input_full_name').val(data[i].employee_name);
						$('#input_phone').val(data[i].employee_phone);
						$('#input_email').val(data[i].employee_email);
						$('#input_place_birth').val(data[i].employee_birth_place);
						$('#input_date_birth').val(data[i].employee_birth_date);
						$('#input_address').val(data[i].employee_address);
						if(data[i].employee_gender == "Laki-laki"){
							$('#male_gender').prop('checked', true);
						}else if(data[i].employee_gender == "Perempuan"){
							$('#female_gender').prop('checked', true);
						}
					}
				},
				error:function(data){
					console.log(data);
				}
			});
			$('#employeeModal').modal('show');
		});
	}

});
