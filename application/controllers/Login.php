<?php

defined('BASEPATH') OR exit("No dirrect script access allowed");

class Login extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$post = $this->input->post();
		$data = [
			'title' => "Login User"
		];
		$this->load->view('app_pages/login', $data);
	}

	public function auth()
	{
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('username','Username','trim|required');

		$post = [
			'username' => $this->input->post('username', TRUE),
			'password' => md5($this->input->post('password', TRUE))
		];

		if($this->form_validation->run() == TRUE)
		{
			$data = $this->Auth_model->login($post);
			echo json_encode($data);
		}else{
			$data = $this->form_validation->error_array();
			echo json_encode($data);
		}
	}
}