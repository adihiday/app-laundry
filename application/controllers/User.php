<?php

defined('BASEPATH') OR exit("No dirrect script access allowed");

class User extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
	}

	public function index()
	{
		$data = [
			'title' => "Login User"
		];
		$this->load->view('app_pages/user_list', $data);
	}

	public function store($id = null)
	{
		$this->form_validation->set_rules('user_id','User ID','trim|required');
		$this->form_validation->set_rules('username', 'Username','trim|required');
		$this->form_validation->set_rules('password','Password', 'trim|required');

		$post = [
			'user_id' => $this->input->post('user_id'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
		];

		if($this->form_validaion->run() == TRUE){
			if($id==null){
				$data = $this->User_model->create($post);
				echo json_encode($data);
			}else{
				$data = $this->User_model->update($id, $post);
				echo json_encode($data);
			}
		}else{
			$data = $this->form_validation->error_array();
			echo json_encode($data);
		}
	}

	public function show($id)
	{
		echo json_encode($this->User_model->get_user($id));
	}

	public function delete($id)
	{
		$data = $this->User_model->delete_user($id);
		if($data)
		{
			$data = ['result' => true];
			echo json_encode($data);
		}else{
			$data = ['result' => false];
			echo json_encode($data);
		}
	}

}