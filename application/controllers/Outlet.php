<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Outlet_model');
	}

	public function index()
	{
		$data = [
			'page_title' => "Data Outlet"
		];		

		$this->load->view('app_pages/outlet', $data);
	}

	public function show($id =  null)
	{
		if($id){
			$this->Outlet_model->show($id);
		}else{
			$this->Outlet_model->show();
		}
	}

	public function store($id = null)
	{
		$this->form_validation->set_rules('outlet_name', 'Nama Outlet', 'trim|required');
		$this->form_validation->set_rules('outlet_address', 'Nomor Telepon', 'trim|required');
		$this->form_validation->set_rules('outlet_phone', 'Email', 'trim|required');
		$this->form_validation->set_rules('outlet_email', 'Alamat', 'trim|required');

		$post = [
			'outlet_name' => $this->input->post('outlet_name'),
			'outlet_address' => $this->input->post('outlet_address'),
			'outlet_phone' => $this->input->post('outlet_phone'),
			'outlet_email' => $this->input->post('outlet_email')
		];

		if($this->form_validation->run() == TRUE){
			if($id){
				$this->Outlet_model->store($id, $post);
				$data = ['result' => true];
			}else{
				$this->Outlet_model->store($post);
				$data = ['result' => true];
			}
		}else{
			echo json_encode($this->form_validaion->error_array());
		}
	}

	public function delete($id)
	{
		if($id){
			$data = $this->Outlet_model->delete($id);
			if($data){
				$data = ['result'=> true];
			}else{
				$data = ['result'=> false];
			}
		}	
	}

	public function log()
	{

	}

}

/* End of file Outlet.php */
/* Location: ./application/controllers/Outlet.php */