<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laundry_package extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Laundry_packages_model');
	}

  	public function show($id = null)
  	{
	    if($id){
	      	$this->Laundry_package_model->show($id);
	    }else{
	      	$this->Laundry_package_model->show();
	    }
  	}

  	public function store($id = null){
		$this->form_validation->set_rules('package_name','Nama Paket','required|trim');
		$this->form_validation->set_rules('package_name','Harga Paket','required|trim');
		$this->form_validation->set_rules('package_name','Deskripsi Paket','required|trim');

    	if($this->form_validation->run() == TRUE){
			if($id){
				$this->Laundry_package_model->store($id, $post);
			}else{
				$this->Laundry_package_model->store($post);
			}
				
			echo json_encode($this->form_validation->error_array());
		}
	}

	public function delete($id)
	{
		$data = $this->Laundry_package_model->delete($id);
		if($data){
			$data = ['result' => true];
		}else{
			$data = ['result' => false];
		}

		echo json_encode($data);
	}
}

/* End of file Laundry_packages.php */
/* Location: ./application/controllers/Laundry_packages.php */
