<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Outlet_model');
	}

  	public function create()
  	{
    	$data = ['page_title' => "Buat transaksi"];
    	$this->load->view('app_pages/form_transaction', $data);
  	}

  	public function store()
  	{

  	}

  	public function report()
  	{

  	}

  	public function update_status()
  	{

  	}

}

/* End of file Outlet.php */
/* Location: ./application/controllers/Outlet.php */
