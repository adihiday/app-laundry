<?php

defined('BASEPATH') OR exit("No dirrect script access allowed!");

class Employee extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Employee_model');
		$this->load->library('Datatables');
	}

	public function index()
	{
		$data = ['page_title' => "Data Pegawai"];
		$this->load->view('app_pages/employee_list', $data);
	}

	/**
		 * Save employee data
		 * @return array
		 * @return void
		 * @author
	 **/

	public function store($id = null)
	{
		$this->form_validation->set_rules('employee_id_card', 'No. KTP', 'trim|required');
		$this->form_validation->set_rules('employee_id_kk', 'No. KK', 'trim|required');
		$this->form_validation->set_rules('employee_name', 'Nama Lengkap', 'trim|required');
		$this->form_validation->set_rules('employee_phone', 'Nomor Telepon', 'trim|required');
		// $this->form_validation->set_rules('employee_email', 'No. KTP', 'trim|required');
		$this->form_validation->set_rules('date_birth', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('employee_gender', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('place_birth', 'Tempat Lahir', 'trim|required');
		$this->form_validation->set_rules('employee_address', 'Alamat Lengkap', 'trim|required');

		$post = [
			'employee_id_card' => $this->input->post('employee_id_card', true),
			'employee_id_kk' => $this->input->post('employee_id_kk', true),
			'employee_name' => $this->input->post('employee_name', true),
			'employee_phone' => $this->input->post('employee_phone', true),
			'employee_email' => $this->input->post('employee_email', true),
			'employee_address' => $this->input->post('employee_address', true),
			'employee_birth_date' => $this->input->post('date_birth', true),
			'employee_birth_place' => $this->input->post('place_birth', true),
			'employee_gender' => $this->input->post('employee_gender', true),
			'created_date' => date('Y-m-d H:i:S')
		];

		if($this->form_validation->run()==TRUE)
		{
			$data = $this->Employee_model->store($post, $id);
			if($data)
			{
				$data = ['result' => true];
				echo json_encode($data);
			}else{
				$data = ['result' => false];
				echo json_encode($data);
			}
		}else{
			$data = $this->form_validation->error_array();
			echo json_encode($data);
		}
	}

	public function show($request = null)
	{
		$search = $this->input->post('param');
		// var_dump($search, $request); die();
		if($request != 'deleted'){
			echo json_encode($this->Employee_model->show($request));
		}elseif($request == 'deleted' && isset($search) || !empty($search)){
			echo json_encode($this->Employee_model->show($request, $search));
		}else{
			echo json_encode($this->Employee_model->show(NULL, $search));
		}
	}

	public function show_data()
	{
		echo json_encode($this->Employee_model->show_data());
	}

	public function delete($id)
	{
		$data = $this->Employee_model->delete($id);
		if($data){
			$data = ['result' => true];
		}else{
			$data = ['result' => false];
		}

		echo json_encode($data);
	}

	public function restore($id)
	{
		$data = $this->Employee_model->restore($id);
		if($data){
			$data = ['result' => true];
		}else{
			$data = ['result' => false];
		}

	}

}
