<?php

defined('BASEPATH') OR exit("No dirrect script access allowed");

/**
 *
 */
class Tracking extends CI_Controller
{

	function __construct(argument)
	{
		// code...
	}

	public function index($param = null)
	{
		$data = ['page_title' => "Tracking Pesanan Anda"];
		if($param != null){
			$data = $this->Tracking_model->get_status($param);
			echo json_encode($data);
		}else{
			$this->load->view('app_tracking_page/tracking', $data);
		}
	}
	
}
