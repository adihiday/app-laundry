<?php

defined('BASEPATH') OR exit("No dirrect script access allowed");

class Dashboard extends CI_Controller{

	public function index(){
		$data = [
			'page_title' => "App Laundry ". APP_VERSION,
		];

		$this->load->view('app_pages/dashboard', $data);
	}
}