<?php

defined('BASEPATH') OR exit("No dirrect script access allowed");

class Customers extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = ['page_title'];
		$this->load->view('app_pages/customer', $data);
	}

	public function show($id = null)
	{
		if($id){
			$this->Customer_model->show($id);
		}else{
			$this->Customer_model->show();
		}
	}

	public function store()
	{
		$this->form_validation->set_rules('customer_name');
		$this->form_validation->set_rules('customer_phone');
		$this->form_validation->set_rules('customer_address');

		$post = [
			'customer_name' => $this->input->post('customer_name'),
			'customer_phone' => $this->input->post('customer_phone'),
			'customer_address' => $this->input->post('customer_address')
		];

		if($this->form_validation->run() == TRUE){
			if($id){
				$data = $this->Customer_model->store($id, $post);
				if($data){
					$data = ['result' => true];
				}else{
					$data = ['result' => false];
				}
			}else{
				$data = $this->Customer_model->store($post);
				if($data){
					$data = ['result' => true];
				}else{
					$data = ['result' => false];
				}
			}
		}else{
			$data = $this->form_validation->error_array();
		}

		echo json_encode($data);
	}

	public function delete($id)
	{
		if($id){
			$data = $this->Customer_model->delete($id);
			if($data){
				$data = ['result' => true];
			}else{
				$data = ['result' => false];
			}
		}else{
			$data = ['result' => "Something wrong!!!"];
		}

		echo json_encode($data);
	}

}
