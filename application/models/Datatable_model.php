<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable_model extends CI_Model{

  	public function __construct()
  	{
    	parent::__construct();
    	//Codeigniter : Write Less Do More
  	}

	function employees_action($employee_id, $trashed_date)
	{
		return $this->load->view('app_action_view/action_button_employee', array('employee_id' => $employee_id, 'trashed_date' => $trashed_date), true);
		// return $trashed_date;
	}

}
