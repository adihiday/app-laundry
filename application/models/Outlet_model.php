<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outlet_model extends CI_Model {

	private $table = 'outlets';
	private $primaryKey = 'outlet_id';

	public function show($id = null)
	{
		if($id){
			$this->db->where($this->primaryKey, $id);
			return $this->db->get($this->table)->result();
		}else{
			$this->datatables->select("*");
			$this->datatables->from($this->table);
			echo json_encode($this->datatables->generate());
		}
	}

	public function store($id = null, $post)
	{
		if($id){
			$this->db->where($this->table,$id);
			$this->db->update($this->table, $post);
		}else{
			$this->db->insert($this->table, $post);
		}

		return $this->db->affected_rows();
	}

	public function active_nonactive($id)
	{

	}

	public function delete($id)
	{
		$this->db->where('outlet_id', $id);
		$this->db->delete('outlets');
		return $this->db->affected_rows();
	}
	

}

/* End of file Outlet_model.php */
/* Location: ./application/models/Outlet_model.php */