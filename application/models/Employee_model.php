<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	private $table = 'employees';
	private $table_outlet = 'outlets';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Datatable_model');
	}

	public function store($post, $id=null)
	{
		if($id == null){
			$this->db->insert($this->table, $post);
		}else{
			$this->db->where('employee_id', $id);
			$this->db->update($this->table, $post);
		}
		return $this->db->affected_rows();
	}

	public function show($request=null, $search=null)
	{
		$or_like = ['employee_name'=> $search, 'employee_address'=> $search, 'employee_phone' => $search, 'employee_email'=> $search]; 
		if($request != 'deleted' && $request != null){
			$this->db->where('employee_id', $request);
			return $this->db->get($this->table)->result();
		}else{
			$this->load->library('Datatables');
			$this->datatables->select("employee_id, employee_name, employee_address, employee_phone,employee_gender, employee_email, IFNULL(trashed_date, '') as trashed_date, $this->table_outlet.outlet_name");
			$this->datatables->join($this->table_outlet, $this->table_outlet.'.outlet_id ='.$this->table.'.outlet_id','left');
			$this->datatables->from($this->table);
			if($request == 'deleted'){
				// $this->db->group_start();
				if(isset($search) && !empty($search)){ 
					$this->db->like('employee_name', $search);
					$this->db->or_like($or_like);
				}
				// $this->db->group_end();
				$this->datatables->where('trashed_date !=', NULL);
				$this->datatables->add_column('action',array($this->Datatable_model, 'employees_action'), 'employee_id, trashed_date');
			}elseif($request == null){
				// $this->db->group_start();
				if(isset($search) && !empty($search)){ 
					$this->db->like('employee_name', $search);
					$this->db->or_like($or_like);
				}
				// $this->db->group_end();
				$this->datatables->where('trashed_date', NULL);
				$this->datatables->add_column('action',array($this->Datatable_model, 'employees_action'), 'employee_id, trashed_date');
			}
			$this->datatables->default_order('employee_id','desc');
			return $this->datatables->generate();
		}
	}

	public function show_data()
	{
		$this->load->library('Datatables');
		$this->datatables->select("*");
		$this->datatables->from($this->table);
		return $this->datatables->generate();
	}

	public function delete($id)
	{
		$this->db->where('employee_id', $id);
		$data = $this->db->get('employees')->row_array();
		if($data['trashed_date'] == NULL){
			$this->db->where('employee_id', $data['employee_id']);
			$this->db->update('employees', array('trashed_date' => date('Y-m-d H:i:s')));
		}elseif($data['trashed_date'] != NULL){
			$this->db->where('employee_id', $data['employee_id']);
			$this->db->delete('employees');
		}
		return $this->db->affected_rows();
	}

	public function restore($id)
	{
		if($id){
			$this->db->where('employee_id', $id);
			$this->db->update('employees', array('trashed_date' => NULL));
			return $this->db->affected_rows();
		}
		
	}
}

/* End of file Employee_model.php */
/* Location: ./application/models/Employee_model.php */
