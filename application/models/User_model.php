
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	private $table = 'users';
	private $primaryKey = 'user_id';

	public function store($id = null, $post)
	{
		if($id){
			$this->db->where('user_id', $id);
			$this->db->update('user');
		}else{
			$this->db->insert('users', $post);
		}
	}

	public function show($id = null)
	{
		if($id){
			$this->db->where($this->primaryKey, $id);
			$this->db->get('users');
		}else{
			$this->db->get($this->table);
		}
	}

	public function active($id)
	{
		$this->db->where($this->primaryKey, $id);
		$data = $this->db->get('users')->row_array();
		if($data['active'] == true){
			$this->db->where($this->primaryKey, $data['user_id']);
			$this->db->update($table, array('active'=>false));
		}elseif($data['active'==false]){
			$this->db->where($this->primaryKey, $data['user_id']);
			$this->db->update($table, array('active'=>true));
		}
	}

	public function delete($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete('users');
	}

	public function activity()
	{

	}

	public function change_password()
	{

	}
	
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */