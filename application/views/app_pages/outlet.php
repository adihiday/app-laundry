<?php $this->load->view('app_main/app_sidebar'); ?>
  <!-- Page content -->
    <style>
        .card_outlet{
            cursor: pointer;
            border-left: 3px solid tomato;
        }
    </style>
  <div class="container-fluid mt--6" id="pages_property" page="outlet" ajax_url="<?php echo base_url('/outlet/show/'); ?>">
    <div class="row">
      <div class="col-xl-12">
        <div class="card">
          <div class="card-header border-0">
            <div class="col-auto mb-2" style="padding: 0px;">
              <b class="text-primary"><?php echo $page_title; ?></b>
              <p>Manajemen data Outlet</p>
              <hr style="margin-top: 0px; padding: 0px;">
            </div>

            <div class="row align-items-center">
              <div class="col-md-3">
                <input type="text" class="form-control input-text" placeholder="Cari">
              </div>
              <div class="col text-right">
                <button data-toggle="modal" id="add_employee" data-target="#employeeModal" class="btn btn-sm btn-primary"><i class="ni ni-shop"></i> Tambah Outlet</button>
                <a href="#!" id="deleted_employees" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Trash</a>
              </div>
            </div>
          </div>
            <div class="col-md-12">
                <div class="row">
                    <?php for($i=0; $i<12; $i++){ ?>
                    <div class="col-xl-4 col-xs-4 col-md-6 ">
                        <div class="card card-stats card_outlet">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="ni ni-shop"></i>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Outlet Jagaraksa</h5>
                                        <small style="font-size: 8pt;"><i class="fa fa-phone"></i> 085841067625</small>
                                        <hr style="margin: 10px 0px 10px 0px;">
                                        <a class="btn btn-success btn-sm text-right" href="/">Edit</a>
                                        <a class="btn btn-danger btn-sm text-right" href="/">Hapus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Modal -->
    <div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="employeeModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-primary" id="employeeModalLongTitle"><i class="fa fa-user-plus"></i> Tambah Pegawai</h5>
            <hr>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="margin-top: -60px !important;margin-bottom: -60px !important;">
            <form action="<?php echo base_url('employee/store/'); ?>" method="post" id="form_employee">
              <div class="row">
                <div class="form-group col-md-12">
                  <label class="form-control-label">NIK <small class="text-danger error_validation" id="nik"> </small></label>
                  <input type="hidden" id="employee_id" class="form-control" name="employee_id">
                  <input type="text" id="input_employee_id_card" name="employee_id_card" class="form-control input-text" placeholder="16 Digit NIK">
                </div>
                
                <div class="form-group col-md-6">
                  <label class="form-control-label">Telepon <small class="text-danger error_validation" id="phone"> </small></label>
                  <input type="text" id="input_phone" name="employee_phone" class="form-control input-text" placeholder="Telepon">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">E-Mail (Optional) <small class="text-danger error_validation" id="email"> </small></label>
                  <input type="text" id="input_email" name="employee_email" class="form-control input-text" placeholder="Telepon">
                </div>
            
                <div class="form-group col-md-12">
                  <label class="form-control-label">Alamat Lengkap <small class="text-danger error_validation" id="address"> </small></label>
                  <textarea type="text" id="input_address" name="employee_address" rows="2" class="form-control" placeholder="RT/RW Desa, Kecamatan Kabupaten, Propinsi"></textarea>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="save_employee_data" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('app_main/app_footer'); ?>
