<?php $this->load->view('app_main/app_sidebar'); ?>

  <!-- Page content -->
  <div class="container-fluid mt--6" id="pages_property" page="employees" ajax_url="<?php echo base_url('/employee/show/'); ?>">
    <div class="row">
      <div class="col-xl-12">
        <div class="card">
          <div class="card-header border-0">
            <div class="col-auto mb-2" style="padding: 0px;">
              <b class="text-primary">Data Pegawai</b>
              <p>Manajemen data pegawai</p>
              <hr style="margin-top: 0px; padding: 0px;">
            </div>

            <div class="row align-items-center">
              <div class="col-md-3">
                <input type="text" active-employee id="search_employee" class="form-control input-text" placeholder="Cari">
              </div>
              <div class="col">
                <button class="btn btn-primary btn-sm" id="active_employees"><i class="fa fa-users"></i> Data Pegawai</button>
                <button class="btn btn-success btn-sm"><i class="fa fa-file-excel"></i> Excel</button>
                <button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf"></i> PDF</button>
              </div>
              <div class="col text-right">
                <button data-toggle="modal" id="add_employee" data-target="#employeeModal" class="btn btn-sm btn-primary"><i class="fa fa-user-plus"></i> Tambah Pegawai</button>
                <a href="#!" id="deleted_employees" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Trash</a>
              </div>
            </div>
          </div>
          <div class="row hidden" id="hot_info">
            <div class="col-md-12">
              <div class="alert alert-primary">
                <i class="fa fa-recycle"></i> Klik tombol restore akan mengembalikan ke list data pegawai.
                <i class="fa fa-trash"></i> Klik tombol restore akan mengembalikan ke list data pegawai.
              </div>
            </div>
        </div>
          <div class="table-responsive pb-4">
            <!-- Projects table -->
            <table class="table align-items-center table-flush table-stripped table-hover" id="table_employees">
              <thead class="thead-light">
                <tr>
                  <th scope="col" class="text-dark">#</th>
                  <th scope="col" class="text-dark">Nama Pegawai</th>
                  <th scope="col" class="text-dark">Telepon </th>
                  <th scope="col" class="text-dark">Posisi</th>
                  <th scope="col" class="text-dark">Outlet</th>
                  <th scope="col" class="text-dark">Alamat</th>
                  <th scope="col" class="text-dark text-center"> Opsi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="employeeModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-primary" id="employeeModalLongTitle"><i class="fa fa-user-plus"></i> Tambah Pegawai</h5>
            <hr>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="margin-top: -60px !important;margin-bottom: -60px !important;">
            <form action="<?php echo base_url('employee/store/'); ?>" method="post" id="form_employee">
              <div class="row">
                <div class="form-group col-md-6">
                  <label class="form-control-label">NIK <small class="text-danger error_validation" id="nik"> </small></label>
                  <input type="hidden" id="employee_id" class="form-control" name="employee_id">
                  <input type="text" id="input_employee_id_card" name="employee_id_card" class="form-control input-text" placeholder="16 Digit NIK">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">No. KK </label>
                  <input type="text" id="input_id_kk" name="employee_id_kk" class="form-control input-text" placeholder="Nomor Kartu Keluarga">
                  <small class="text-danger error_validation" id="kk"> </small>
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">Nama Lengkap <small class="text-danger error_validation" id="full_name"></small></label>
                  <input type="text" id="input_full_name" name="employee_name" class="form-control input-text" placeholder="Nama Lengkap Sesuai KTP">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">Jenis Kelamin <small class="text-danger error_validation" id="gender"> </small></label><br>
                  <label><input id="male_gender" type="radio" class="radio-input" name="employee_gender" value="Laki-laki"> Laki-laki</label>
                  <label><input id="female_gender" type="radio" class="radio-input" name="employee_gender" value="Perempuan"> Perempuan</label>
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">Telepon <small class="text-danger error_validation" id="phone"> </small></label>
                  <input type="text" id="input_phone" name="employee_phone" class="form-control input-text" placeholder="Telepon">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">E-Mail (Optional) <small class="text-danger error_validation" id="email"> </small></label>
                  <input type="text" id="input_email" name="employee_email" class="form-control input-text" placeholder="Telepon">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">Tempat Lahir <small class="text-danger error_validation" id="birth_place"> </small></label>
                  <input type="text" id="input_place_birth" name="place_birth" class="form-control input-text" placeholder="Tempat Lahir">
                </div>
                <div class="form-group col-md-6">
                  <label class="form-control-label">Tanggal Lahir <small class="text-danger error_validation" id="birth_date"> </small></label>
                  <input type="date" id="input_date_birth" name="date_birth" class="form-control input-text" placeholder="Telepon">
                </div>
                <div class="form-group col-md-12">
                  <label class="form-control-label">Alamat Lengkap <small class="text-danger error_validation" id="address"> </small></label>
                  <textarea type="text" id="input_address" name="employee_address" rows="2" class="form-control" placeholder="RT/RW Desa, Kecamatan Kabupaten, Propinsi"></textarea>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="save_employee_data" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <?php $this->load->view('app_main/app_footer'); ?>
