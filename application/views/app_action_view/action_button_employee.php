
<?php if($trashed_date == 0){ ?>
	<a class="btn btn-success btn-sm" id="edit_employee" href="<?php echo base_url('/employee/show/').$employee_id; ?>"><i class="fa fa-edit"></i> Edit</a>
	<a class="btn btn-primary btn-sm" href="<?php echo base_url('/employee/show/').$employee_id; ?>"><i class="fa fa-list"></i> Detail</a>
	<a id="delete_employee" class="btn btn-danger btn-sm" href="<?php echo base_url('/employee/delete/').$employee_id; ?>"><i class="fa fa-trash"></i> Sampah</a>
<?php }else{ ?>
	<a class="btn btn-success btn-sm" id="restore_employee" href="<?php echo base_url('/employee/restore/').$employee_id; ?>"><i class="fa fa-recycle"></i> Restore</a>
	<button type="button" class="btn btn-danger btn-sm" name="button"><i class="fa fa-trash"></i> Hapus</button>
<?php } ?>
